#!/bin/sh
#
### BEGIN INIT INFO
# Provides:          bindgraph
# Required-Start:    $syslog $network $remote_fs
# Required-Stop:     $syslog $remote_fs
# Should-Start:      $local_fs $named
# Should-Stop:       $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: BIND graph
# Description:       Create graphs from DNS activity
### END INIT INFO
#
# initscript for bindgraph
# (C) 2006 Jose Luis Tallon <jltallon@adv-solutions.net>
# based upon <skeleton>, code by Ian Murdock and others

CONFIG=/etc/default/bindgraph
DNS_LOG=/var/log/bind9-query.log

NAME=bindgraph
DESC="DNS Statistics"
DAEMON=/usr/sbin/bindgraph.pl

PIDFILE=/var/run/servergraph/bindgraph.pid
CACHEDIR=/var/cache/bindgraph
RRD_DIR=/var/lib/bindgraph
RRD_NAME=bindgraph

test -x $DAEMON || exit 0

if [ -f $CONFIG ]; then
        . $CONFIG
fi

# Just to be sure it is there...
pid_dir=`dirname $PIDFILE`
test -d $pid_dir || mkdir -p $pid_dir
chown daemon:root $pid_dir


. /lib/lsb/init-functions

if [ -n "$LOG_FORMAT" ]; then
	FORMAT="--format=${LOG_FORMAT}"
fi

case "$1" in
  start)
	log_daemon_msg "Starting $DESC" "$DAEMON"
	if start-stop-daemon --start --quiet --exec $DAEMON -N 15 -c daemon:adm -- \
		-l $DNS_LOG $FORMAT -d --daemon_rrd=$RRD_DIR --rrd_name=$RRD_NAME $ARGS ;
	then
		log_end_msg 0
	else
		log_end_msg $?
	fi
  ;;

  stop)
	log_daemon_msg "Stopping $DESC: " "$DAEMON"
	start-stop-daemon --stop --oknodo --pidfile $PIDFILE -c daemon:adm ;
	st=$?
	rm -f $PIDFILE
	log_end_msg $st
  ;;

  restart|force-reload)
	$0 stop
	sleep 1
	$0 start
  ;;

  status)
  	if pidofproc -p $PIDFILE $DAEMON ; then
		log_success_msg "$DAEMON is running"
		exit 0
	else
		log_success_msg "$DAEMON is not running"
		exit 3
	fi	                                                              	
  ;;

  *)
    log_success_msg "Usage: $0 start|stop|restart|force-reload"
    exit 0
    ;;
esac

exit 0
